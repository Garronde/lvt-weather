import datetime

import db_helpers as db

def init():
    now = datetime.datetime.now()
    today = str(now.date())

    db.create_table_temp()
    db.create_table_score()
    db.update_daily_score(0,today,0)
    print("lvt-weather db created and initialised")

if __name__ == '__main__': 
    init()