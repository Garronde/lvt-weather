'''
Lyon versus Toulouse weather app
'''

import datetime
import requests
import json
import os
from colorama import Fore
import subprocess as sp

import db_helpers as db
import telegram_helpers as tg
import initdb

import personal_helpers as perso

#Constants
api_weather = 'https://api.openweathermap.org/data/2.5/weather'
api_opt_metric = '&units=metric'
api_opt_key = perso.get_weather_api()

def init():
    #initdb.init()
    print('\n  _       _______  __          __        _   _               \n | |     |__   __| \ \        / /       | | | |              \n | |  __   _| |     \ \  /\  / /__  __ _| |_| |__   ___ _ __ \n | |  \ \ / / |      \ \/  \/ / _ \/ _` | __| \'_ \ / _ \ \'__|\n | |___\ V /| |       \  /\  /  __/ (_| | |_| | | |  __/ |   \n |______\_/ |_|        \/  \/ \___|\__,_|\__|_| |_|\___|_|\n\n')

def get_city_id(city):
    if city.strip() == "Lyon":
        city_id = 2996944
    elif city.strip() == "Toulouse":
        city_id = 2972315
    else:
        print(f"{Fore.RED}WARNING :: city id identification failed, please check http://bulk.openweathermap.org/sample/")
        exit(1)

    return city_id
    
def main():
    content = ''


    init()
    #content += title

    now = datetime.datetime.now()
    year = str(now.year)
    # getting and saving temps
    temp_content_lyon = get_temp('Lyon')
    temp_content_toulouse = get_temp('Toulouse')

    content += '\n'+temp_content_lyon + '\n'+temp_content_toulouse

    comparison_today = comparison()
    content += '\n'+comparison_today
        
    score_today = update_score()
        
    analysis_today = analyse(score_today)
    content += '\n'+analysis_today

    #if it is sunday (6 is sunday wwekday)
    if now.weekday() == 6:
        weekly_analysis = weekly_analyse()
        content += '\n'+weekly_analysis

    if now.day == 1:
        monthly_analysis = monthly_analyse()
        content += '\n'+monthly_analysis


    tg.send(content)

def get_temp(city):
    now = datetime.datetime.now()
    date = str(now.date())
    city_id = get_city_id(city)
    weather_request = requests.get(api_weather + f'?id={city_id}' + api_opt_metric +api_opt_key)
    temp_content = ''

    #print(f"DEBUG : {weather_request.text}")

    if weather_request.status_code == 200:
        temp = weather_request.json()["main"]["temp"]
        temp_content += f'Temp in {city} is {temp}°C'
        db.add_temp(temp,date,city)

        print(temp_content)
    else:
        print(f"{Fore.RED}WARNING :: api call failed\nDEBUG :: api call response: {weather_request.text}")
        exit()

    return temp_content

def comparison():
    now = datetime.datetime.now()
    today = str(now.date())
    #print(f"Today : {today}")
    
    temp_lyon_today = db.get_temp_by_city_date('lyon',today)
    temp_toulouse_today = db.get_temp_by_city_date('toulouse',today)
    
    #print(temp_lyon_today)
    #print(temp_toulouse_today)
    
    value = round(temp_lyon_today-temp_toulouse_today,2)
    db.save_diff(value,today)

    if temp_lyon_today > temp_toulouse_today:
        comparison = f'It is {round(temp_lyon_today-temp_toulouse_today,2)}°C hotter in Lyon today'
    elif temp_lyon_today < temp_toulouse_today:
        comparison = f'It is {round(temp_toulouse_today-temp_lyon_today,2)}°C hotter in Toulouse today'
    else: 
        comparison = f'It is the exact same Temp in Lyon and Toulouse Today WOW'
    
    print(comparison)

    return comparison
    
def update_score():
    now = datetime.datetime.now()
    today = str(now.date())
    #print(f"Today : {today}")
    
    temp_lyon_today = db.get_temp_by_city_date('lyon',today)
    temp_toulouse_today = db.get_temp_by_city_date('toulouse',today)
    #print(temp_lyon_today)
    #print(temp_toulouse_today)
    
    if temp_lyon_today > temp_toulouse_today:
        last_score = db.get_current_score()
        current_score = last_score + 1
        db.update_daily_score(1,today,current_score)
    elif temp_lyon_today < temp_toulouse_today:
        last_score = db.get_current_score()
        current_score = last_score - 1
        db.update_daily_score(-1,today,current_score)
    else: 
        last_score = db.get_current_score()
        current_score = last_score
        db.update_daily_score(0,today,current_score)
    
    score_today = db.get_current_score()

    #print(score_today)

    return score_today

def analyse(score):

    now = datetime.datetime.now()
    year = str(now.year)

    if score > 0:
        #lyon is ahead
        if score == 1:
            score_analysis = f"There is {abs(score)} day in {year} when Lyon whas the hottest"
        else:
            score_analysis = f"There were {abs(score)} days in {year} when Lyon whas the hottest"
    elif score < 0:
        #lyon is behind
        if score == -1:
            score_analysis = f"There is {abs(score)} day in {year} when Toulouse whas the hottest"
        else:
            score_analysis = f"There were {abs(score)} days in {year} when Toulouse whas the hottest"
    else:
        score_analysis = f"Lyon and Toulouse have been the hottest each one the exact same amount of days in {year} ! Wow"

    print(score_analysis)

    return score_analysis


def weekly_analyse():
    
    now = datetime.datetime.now()
    today = now.date()
    last_week_date = today - datetime.timedelta(days=7)

    avg_temp_lyon = db.get_avg_temp_by_city('lyon', str(today), str(last_week_date))
    avg_temp_toulouse = db.get_avg_temp_by_city('toulouse', str(today), str(last_week_date))

    #print(avg_temp_lyon)
    #print(avg_temp_toulouse)

    weekly_analysis = f'\nThis week the average temp in Lyon was {round(avg_temp_lyon,2)}°C\nThis week the average temp in Toulouse was {round(avg_temp_toulouse,2)}°C'

    print(weekly_analysis)

    return weekly_analysis

def monthly_analyse():

    now = datetime.datetime.now()
    today = now.date()
    first_day_of_year = datetime.date(datetime.date.today().year, 1, 1)

    temp_diff = db.get_max_temp_diff(today,first_day_of_year)

    max_temp_diff = float(temp_diff[0].replace(',','.'))


    if max_temp_diff < 0:
        monthly_analysis = f'\nThe greatest temperature difference this year was {abs(max_temp_diff)}°C\nToulouse was the hottest, it happened on the {temp_diff[1]}'
    elif max_temp_diff > 0:
        monthly_analysis = f'\nThe greatest temperature difference this year was {max_temp_diff}°C\nLyon was the hottest, it happened on the {temp_diff[1]}'
    else:
        monthly_analysis = ''
        print('Wow this strange')

    print(monthly_analysis)

    return monthly_analysis

if __name__ == '__main__': 
    sp.call('clear',shell=True)
    main()
