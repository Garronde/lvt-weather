import requests
import urllib.parse

import personal_helpers as perso
# You need to create a file personal_helpers.py in it :
#def get_bot_token():
#    return 'yourtoken'
#def get_chat_id():
#    return 'yourchatid'


def send(bot_message):
    
    bot_token = perso.get_bot_token()
    bot_chatID = perso.get_chat_id()
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + urllib.parse.quote_plus(bot_message)

    response = requests.get(send_text)

    return response.json()
    

