# LvT Weather

Weather app for competition between Lyon and Toulouse

# functionalities
- Get the current temp in Lyon an Toulouse
- Compare them
- Keep score of which was the hottest
- Send results
- Weekly report of average week temp
- DB local backup
- Monthly report of greatest temp diff

# starting the first db
- rename starting db: `cp starting-weather.db weather.db`

# Technologies
- Weather API: https://api.openweathermap.org/data/2.5/weather
- Telegram Bot
- SQlite Database

# Setup on Rasberry Pi
- Use cron to run 
- Use cron to backup db

# Cron tutorial
edit the cron to run the app: `crontab -e` for exemple:

    0 13 * * * python3 /home/pi/lvt-weather/lvt-weather.py
    0 14 * * * python3 /home/pi/lvt-weather/db_archive.py

# Create personal_helpers.py file to store API keys
    def get_bot_token():
        return 'YOURTOKEN'

    def get_chat_id():
        return 'CHATID'

    def get_weather_api():
        return 'APITOKEN'

# TODO
- Email db