#!/bin/bash

DATE=`date +%Y-%m-%d`
archive=/home/pi/lyon-vs-toulouse-weather-dbarchive-DONOTDELETE/$DATE

mkdir -p $archive

rsync -rtvuh weather.db $archive

exit 0
