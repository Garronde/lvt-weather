import sqlite3
from colorama import Fore

##TODO : SELECT * FROM temp WHERE city="Lyon";
##TODO : SELECT AVG(value), city FROM temp GROUP BY city;
##TODO : SELECT 'The avg temp in ' || city || ' is ' || AVG(value) || 'C' FROM temp GROUP BY city;
##TODO : SELECT * FROM temp WHERE date BETWEEN '2020-11-20' AND '2020-11-25'
##TODO : SELECT 'The avg temp in ' || city || ' is ' || round(AVG(value),1) || '°C' FROM temp WHERE date BETWEEN '2020-11-20' AND '2020-11-28'  GROUP BY city 

pathpi = '/home/pi/lvt-weather/'
'''

  _______                     _______    _     _      
 |__   __|                   |__   __|  | |   | |     
    | | ___ _ __ ___  _ __      | | __ _| |__ | | ___ 
    | |/ _ \ '_ ` _ \| '_ \     | |/ _` | '_ \| |/ _ \
    | |  __/ | | | | | |_) |    | | (_| | |_) | |  __/
    |_|\___|_| |_| |_| .__/     |_|\__,_|_.__/|_|\___|
                     | |                              
                     |_|                              

'''    

def create_table_temp():
    conn = sqlite3.connect(pathpi+'weather.db')

    cursor = conn.cursor()

    query = '''
        CREATE TABLE IF NOT EXISTS temp(
            id INTEGER PRIMARY KEY, 
            value FLOAT,
            date TEXT NOT NULL,
            city TEXT NOT NULL
        )
    '''
    #date TEXT as strings ("YYYY-MM-DD")

    cursor.execute(query)

    conn.commit()
    conn.close()

def add_temp(value,date,city):
    conn = sqlite3.connect(pathpi+'weather.db')

    cursor = conn.cursor()

    query = '''
        INSERT OR IGNORE INTO temp(value,date,city)
                    VALUES ( ?,?,? )
    '''

    cursor.execute(query,(value,date,city))

    conn.commit()
    conn.close()

def get_all_temp():
    conn = sqlite3.connect(pathpi+'weather.db')

    cursor = conn.cursor()

    query = '''
        SELECT id,value,date,city
        FROM temp
    '''

    cursor.execute(query)
    all_rows = cursor.fetchall()

    conn.commit()
    conn.close()

    return all_rows

def get_temp_by_city_date(city,today):
    
    #adding wildcard for date
    today += '%'

    conn = sqlite3.connect(pathpi+'weather.db')

    cursor = conn.cursor()
    query = "SELECT value FROM temp WHERE city LIKE ? AND date like ?"
    cursor.execute(query,(city,today))
    temp_of_day = cursor.fetchone()

    conn.commit()
    conn.close()

    if not temp_of_day:
        return None
    return temp_of_day[0]
    
def get_avg_temp_by_city(city,today,lastweek):
    
    conn = sqlite3.connect(pathpi+'weather.db')

    cursor = conn.cursor()
    query = "SELECT AVG(value) FROM temp WHERE city like ? AND date BETWEEN ? AND ?" 
    cursor.execute(query,(city,lastweek,today))
    avg_temp = cursor.fetchone()

    conn.commit()
    conn.close()

    if not avg_temp:
        return None
    return avg_temp[0]

'''

   _____                      _______    _     _      
  / ____|                    |__   __|  | |   | |     
 | (___   ___ ___  _ __ ___     | | __ _| |__ | | ___ 
  \___ \ / __/ _ \| '__/ _ \    | |/ _` | '_ \| |/ _ \
  ____) | (_| (_) | | |  __/    | | (_| | |_) | |  __/
 |_____/ \___\___/|_|  \___|    |_|\__,_|_.__/|_|\___|

'''


def create_table_score():
    conn = sqlite3.connect(pathpi+'weather.db')

    cursor = conn.cursor()

    query = '''
        CREATE TABLE IF NOT EXISTS score(
            id INTEGER PRIMARY KEY, 
            value INTEGER,
            date TEXT NOT NULL UNIQUE,
            current INTEGER
        )
    '''
    #date TEXT as strings ("YYYY-MM-DD")

    cursor.execute(query)

    conn.commit()
    conn.close()

def update_daily_score(value,date,current):
    conn = sqlite3.connect(pathpi+'weather.db')

    cursor = conn.cursor()

    query = '''
        INSERT OR IGNORE INTO score(value,date,current)
                    VALUES ( ?,?,? )
    '''

    cursor.execute(query,(value,date,current))

    conn.commit()
    conn.close()


def get_current_score():
    conn = sqlite3.connect(pathpi+'weather.db')

    cursor = conn.cursor()

    query = '''
        SELECT current FROM score ORDER BY id DESC
    '''

    cursor.execute(query)
    current_score = cursor.fetchone()

    conn.commit()
    conn.close()

    if not current_score:
        return None
    return current_score[0]

# Temp Diff Table

'''
CREATE TABLE IF NOT EXISTS tempdiff(
            id INTEGER PRIMARY KEY, 
            value FLOAT,
            date TEXT NOT NULL
        )
'''

def save_diff(value,date):
    conn = sqlite3.connect(pathpi+'weather.db')

    cursor = conn.cursor()

    query = '''
        INSERT OR IGNORE INTO tempdiff(value,date)
                    VALUES ( ?,? )
    '''

    cursor.execute(query,(value,date))

    conn.commit()
    conn.close()

def get_max_temp_diff(today,firstdayofyear):
    
    conn = sqlite3.connect(pathpi+'weather.db')

    cursor = conn.cursor()
    query = "SELECT MAX(value), date FROM tempdiff WHERE date BETWEEN ? AND ?" 
    cursor.execute(query,(firstdayofyear,today))
    max_diff_temp = cursor.fetchone()

    conn.commit()
    conn.close()

    if not max_diff_temp:
        return None
    return max_diff_temp

def main():
    print(f"{Fore.RED}WARNING :: Please use lvt-weather.py")
    exit(1)

if __name__ == '__main__':
    main()